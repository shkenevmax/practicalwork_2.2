﻿// PracticalWork_2.2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>

using namespace std;

int HashFunc(string& NewWords)
{
	int ControlSum = 0;
	for (string::size_type i = 0; i < NewWords.size(); i++)
	{
		ControlSum += NewWords[i];
	}
	return ControlSum % 99;
}

int main()
{
	string KeyString;
	cin >> KeyString;
	cout << endl;

	cout << HashFunc(KeyString) << endl;
}